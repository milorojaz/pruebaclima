package com.camilorojas.pruebaclima;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.camilorojas.pruebaclima.API.model.DailyModel;

import java.util.List;

import de.mateware.snacky.Snacky;

/**
 * Created by root on 6/03/18.
 */

public class AdapterClimaSemanal extends RecyclerView.Adapter<AdapterClimaSemanal.AdapterHolder> {

    List<DailyModel> listdiasclima;
    Context cn;
    Fecha fecha;
    public AdapterClimaSemanal(List<DailyModel> listdiasclima,Context cn){
        this.listdiasclima = listdiasclima;
        this.cn=cn;
    }

    @Override
    public AdapterClimaSemanal.AdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_clima_sem, parent, false);
        AdapterClimaSemanal.AdapterHolder holder = new AdapterClimaSemanal.AdapterHolder(itemView);
        return holder;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onBindViewHolder(AdapterClimaSemanal.AdapterHolder holder, int position) {
        fecha = new Fecha(cn);
        holder.tvdia.setText(""+fecha.getFechafromTimestamps(listdiasclima.get(position).getTime()));
        double tempsmax = (listdiasclima.get(position).getTemperaturaMaxima()-32) * 5/9.0;
        double tempmin = (listdiasclima.get(position).getTemperaturaMinima()-32) * 5/9.0;


        holder.tvTempMax.setText(listdiasclima.get(position).getTemperaturaMaxima() + "F - "+String.format("%.2f", tempsmax) +"°");
        holder.tvTempMin.setText(listdiasclima.get(position).getTemperaturaMinima() + "F - "+String.format("%.2f", tempmin) +"°");

        switch (listdiasclima.get(position).getIcon()){
            case "clear-day":
                holder.IconDia.setBackgroundResource(R.drawable.ic_sunny);
                break;
            case "clear-night":
                holder.IconDia.setBackgroundResource(R.drawable.ic_clear_night);
                break;
            case "rain":
                holder.IconDia.setBackgroundResource(R.drawable.ic_rain);
                break;
            case "snow":
                holder.IconDia.setBackgroundResource(R.drawable.ic_snow);
                break;
            case "sleet":
                holder.IconDia.setBackgroundResource(R.drawable.ic_snow);
                break;
            case "wind":
                holder.IconDia.setBackgroundResource(R.drawable.ic_wind);
                break;
            case "fog":
                holder.IconDia.setBackgroundResource(R.drawable.ic_fog);
                break;
            case "cloudy":
                holder.IconDia.setBackgroundResource(R.drawable.ic_cloud);
                break;
            case "partly-cloudy-day":
                holder.IconDia.setBackgroundResource(R.drawable.ic_partly_cloudy);
                break;
            case "partly-cloudy-night":
                holder.IconDia.setBackgroundResource(R.drawable.ic_clear_night);
                break;
            case "hail":
                holder.IconDia.setBackgroundResource(R.drawable.ic_hail);
                break;
            case "thunderstorm":
                holder.IconDia.setBackgroundResource(R.drawable.ic_thunderstorm);
                break;
            case "tornado":
                holder.IconDia.setBackgroundResource(R.drawable.ic_tornado);
                break;
            default:
                holder.IconDia.setBackgroundResource(R.drawable.ic_cloud);

                break;
        }
    }


    @Override
    public int getItemCount() {
        return listdiasclima.size();
    }

    public class AdapterHolder extends RecyclerView.ViewHolder {

        private final TextView tvdia;
        private final CardView cvDia;
        private final TextView tvTempMax;
        private final TextView tvTempMin;
        private final ImageView IconDia;

        public AdapterHolder(View itemView) {
            super(itemView);
            tvdia = (TextView) itemView.findViewById(R.id.idTextviewDia);
            cvDia = (CardView) itemView.findViewById(R.id.idCardViewDia);
            tvTempMax = (TextView) itemView.findViewById(R.id.idTextViewTemperaturaMax);
            tvTempMin = (TextView) itemView.findViewById(R.id.idTextViewTemperaturaMin);
            IconDia = (ImageView) itemView.findViewById(R.id.idIconClima);



        }
    }

}
