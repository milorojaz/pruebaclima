package com.camilorojas.pruebaclima.API.Response;

import com.camilorojas.pruebaclima.API.model.CurrentlyModel;
import com.camilorojas.pruebaclima.API.model.DailyModel;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;

import java.util.List;

/**
 * Created by root on 10/02/18.
 */

public class ResponseWheather {
    double latitude;
    double longitude;
    String timezone;
    JsonElement currently;
    JsonElement minutely;
    JsonElement hourly;
    JsonElement daily;
    JsonElement flags;
    double offset;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public JsonElement getCurrently() {
        return currently;
    }

    public void setCurrently(JsonElement currently) {
        this.currently = currently;
    }

    public JsonElement getMinutely() {
        return minutely;
    }

    public void setMinutely(JsonElement minutely) {
        this.minutely = minutely;
    }

    public JsonElement getHourly() {
        return hourly;
    }

    public void setHourly(JsonElement hourly) {
        this.hourly = hourly;
    }

    public JsonElement getDaily() {
        return daily;
    }

    public void setDaily(JsonElement daily) {
        this.daily = daily;
    }

    public JsonElement getFlags() {
        return flags;
    }

    public void setFlags(JsonElement flags) {
        this.flags = flags;
    }

    public double getOffset() {
        return offset;
    }

    public void setOffset(double offset) {
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "ResponseWheather{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", timezone='" + timezone + '\'' +
                ", currently=" + currently +
                ", minutely=" + minutely +
                ", hourly=" + hourly +
                ", daily=" + daily +
                ", flags=" + flags +
                ", offset=" + offset +
                '}';
    }
}
