package com.camilorojas.pruebaclima.API;

import com.camilorojas.pruebaclima.API.Response.ResponseWheather;


import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by root on 10/02/18.
 */

public interface ApiService {

    @GET("{coordenada}")
    Observable<ResponseWheather> getClima(@Path("coordenada") String coordenada);
}
