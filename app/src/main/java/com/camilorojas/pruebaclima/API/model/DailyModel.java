package com.camilorojas.pruebaclima.API.model;

import java.sql.Array;

/**
 * Created by root on 6/03/18.
 */

public class DailyModel {
    String summary;
    String icon;
    double TemperaturaMinima;
    double TemperaturaMaxima;
    long time;

    public DailyModel(String summary, String icon, double temperaturaMinima, double temperaturaMaxima, long time) {
        this.summary = summary;
        this.icon = icon;
        TemperaturaMinima = temperaturaMinima;
        TemperaturaMaxima = temperaturaMaxima;
        this.time = time;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public double getTemperaturaMinima() {
        return TemperaturaMinima;
    }

    public void setTemperaturaMinima(double temperaturaMinima) {
        TemperaturaMinima = temperaturaMinima;
    }

    public double getTemperaturaMaxima() {
        return TemperaturaMaxima;
    }

    public void setTemperaturaMaxima(double temperaturaMaxima) {
        TemperaturaMaxima = temperaturaMaxima;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
