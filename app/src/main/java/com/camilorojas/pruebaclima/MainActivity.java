package com.camilorojas.pruebaclima;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.camilorojas.pruebaclima.API.ApiAdapter;
import com.camilorojas.pruebaclima.API.GPSTracker;
import com.camilorojas.pruebaclima.API.model.CurrentlyModel;
import com.camilorojas.pruebaclima.API.model.DailyModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.sql.Array;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    LocationManager locationManager;
    double latitude;
    double longitude;
    Context context = this;

    //VARIABLES DE CLIMA
    JsonObject climaactual;
    JsonObject climasemanal;
    List<DailyModel> listaClimadias= new ArrayList<>();
    private AdapterClimaSemanal adapterClimaSem;

    Fecha fecha=new Fecha(this);
    GPSTracker gps;

    //VARIABLES FRONT
    TextView tvclimaactual;
    TextView tvfechaactual;
    LinearLayout LinearCargaData;
    LinearLayout LinearClima;
    TextView tvvelocidadviento;
    TextView tvporcentajehumedad;
    TextView mToolBarTextView;
    ImageView IconClima;
    RecyclerView rvClimaDIas;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        tvclimaactual = (TextView) findViewById(R.id.idTextViewTemperatura);
        LinearCargaData = (LinearLayout) findViewById(R.id.idLayoutCarga);
        LinearClima = (LinearLayout) findViewById(R.id.idLayoutDatos);
        tvvelocidadviento=(TextView) findViewById(R.id.idTextViewVelocidadViento);
        tvporcentajehumedad=(TextView) findViewById(R.id.idTextViewHumedad);
        tvfechaactual=(TextView) findViewById(R.id.idTextViewFechaActual);
        IconClima=(ImageView)findViewById(R.id.idIconClima);
        rvClimaDIas = (RecyclerView) findViewById(R.id.idRecyclerClimaDias);

        rvClimaDIas.setHasFixedSize(true);
        final LinearLayoutManager layoutManagerGalery = new LinearLayoutManager(context);
        layoutManagerGalery.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvClimaDIas.setLayoutManager(layoutManagerGalery);
        adapterClimaSem = new AdapterClimaSemanal(listaClimadias,context);
        rvClimaDIas.setAdapter(adapterClimaSem);
        // SE REALIZA LA PETICION DE PERMISOS NECESARIOS PARA HABILIATAR LAS FUNCIONES
        ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.INTERNET}, 1);


        if(checkLocation()){

        }
        initToolbar();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(checkLocation()){
        }
    }

    private void cargarDatos() {
        double temps = (climaactual.get("temperature").getAsDouble()-32) * 5/9.0;

        tvclimaactual.setText(climaactual.get("temperature") + "F - "+String.format("%.2f", temps) +"°");
        tvfechaactual.setText(fecha.getSemana()+","+fecha.getFechaActual());
        tvporcentajehumedad.setText(climaactual.get("humidity").getAsString()+"%");
        tvvelocidadviento.setText(climaactual.get("windSpeed").getAsString()+ " mph");

        JsonArray climadias = climasemanal.get("data").getAsJsonArray();
        for(int i=0; i<climadias.size();i++){
            JsonObject jobj = climadias.get(i).getAsJsonObject();
            listaClimadias.add(new DailyModel(jobj.get("summary").getAsString(),
                    jobj.get("icon").getAsString(),
                    jobj.get("temperatureMin").getAsDouble(),
                    jobj.get("temperatureMax").getAsDouble(),
                    jobj.get("time").getAsLong()));
        }
        adapterClimaSem.notifyDataSetChanged();

        switch (climaactual.get("icon").getAsString()){
            case "clear-day":
                IconClima.setBackgroundResource(R.drawable.ic_sunny);
                break;
            case "clear-night":
                IconClima.setBackgroundResource(R.drawable.ic_clear_night);
                break;
            case "rain":
                IconClima.setBackgroundResource(R.drawable.ic_rain);
                break;
            case "snow":
                IconClima.setBackgroundResource(R.drawable.ic_snow);
                break;
            case "sleet":
                IconClima.setBackgroundResource(R.drawable.ic_snow);
                break;
            case "wind":
                IconClima.setBackgroundResource(R.drawable.ic_wind);
                break;
            case "fog":
                IconClima.setBackgroundResource(R.drawable.ic_fog);
                break;
            case "cloudy":
                IconClima.setBackgroundResource(R.drawable.ic_cloud);
                break;
            case "partly-cloudy-day":
                IconClima.setBackgroundResource(R.drawable.ic_partly_cloudy);
                break;
            case "partly-cloudy-night":
                IconClima.setBackgroundResource(R.drawable.ic_clear_night);
                break;
            case "hail":
                IconClima.setBackgroundResource(R.drawable.ic_hail);
                break;
            case "thunderstorm":
                IconClima.setBackgroundResource(R.drawable.ic_thunderstorm);
                break;
            case "tornado":
                IconClima.setBackgroundResource(R.drawable.ic_tornado);
                break;
            default:
                IconClima.setBackgroundResource(R.drawable.ic_cloud);

                break;
        }
    }





    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Su ubicación esta desactivada.\npor favor active su ubicación " +
                        "usa esta app")
                .setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }


    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolBarTextView = (TextView) findViewById(R.id.idTextToolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        mToolbar.setNavigationIcon(R.drawable.ic_refresh);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gps = new GPSTracker(context);
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                Log.v("GPS", "\n " + gps.getLongitude() + " " + gps.getLatitude());
                String stringGPS =latitude+","+longitude;
                try {
                    ApiAdapter.getApiService().getClima(stringGPS)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(ResponseWheather -> {

                                LinearClima.setVisibility(View.VISIBLE);
                                LinearCargaData.setVisibility(View.GONE);
                                climasemanal=ResponseWheather.getDaily().getAsJsonObject();
                                climaactual = ResponseWheather.getCurrently().getAsJsonObject();

                                cargarDatos();

                            }, err -> {
                                err.printStackTrace();
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mToolBarTextView.setText("Clima");
    }
}
