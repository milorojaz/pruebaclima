package com.camilorojas.pruebaclima;

import android.annotation.SuppressLint;
import android.content.Context;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by root on 6/03/18.
 */

public class Fecha {
    Context cn;
    public Fecha(Context context) {
        this.cn=context;
    }

    @SuppressLint("SimpleDateFormat")
    public String getFechaActual()
    {
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat("dd MMMM ");
        return hourdateFormat.format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public String getSemana() {
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat("EEEE ");
        return hourdateFormat.format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public String getFechafromTimestamps(long time)
    {
        java.util.Date date=new java.util.Date(time*1000);

        DateFormat hourdateFormat = new SimpleDateFormat("EEEE,dd MMMM ");
        return hourdateFormat.format(date);
    }
}
